<?php

namespace App\Http\Controllers;

use App\PaymentData;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use stdClass;

class UserController extends Controller
{
    public function getStepOne() {

        $data = session('firstForm');
        return view('view1', compact('data'));
    }

    public function getStepTwo() {
        $data = session('secondForm');
        return view('view2', compact('data'));
    }

    public function getStepThree() {

        $data = session('thirdForm');
        return view('view3', compact('data'));
    }

    public function getStepFour() {
        return view('view4');
    }

    public function postStepOne(Request $request) {
        $firstFormObject = new stdClass();
        $firstFormObject->name = $request->name;
        $firstFormObject->lastname = $request->lastname;
        $firstFormObject->phone = $request->phone;
        session(['firstForm' => $firstFormObject]);
        session(['active' => 2]);
        return redirect()->route('getSecond');
    }

    public function postStepTwo(Request $request) {
        $seconFormObject = new stdClass();
        $seconFormObject->address = $request->address;
        $seconFormObject->zipcode = $request->zipcode;
        $seconFormObject->city = $request->city;
        $seconFormObject->houseNumber = $request->houseNumber;
        session(['secondForm' => $seconFormObject]);
        session(['active' => 3]);
        return redirect()->route('getThird');
    }

    public function postStepThree(Request $request) {
        try {
            $thirdFormObject = new stdClass();
            $thirdFormObject->owner = $request->owner;
            $thirdFormObject->IBAN = $request->IBAN;
            session(['thirdForm' => $thirdFormObject]);

            // save user in database
            $firstForm = session('firstForm');
            $secondForm = session('secondForm');
            $user = new User();
            $user->name = $firstForm->name;
            $user->last_name = $firstForm->lastname;
            $user->telephone = $firstForm->phone;
            $user->address = $secondForm->address;
            $user->zip_code = $secondForm->zipcode;
            $user->house_number = $secondForm->houseNumber;
            $user->city = $secondForm->city;
            $user->account_owner = $request->owner;
            $user->IBAN = $request->IBAN;

            if ($user->save()) {
                $wunderApi = new Client();
                $result = $wunderApi->post('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                    'form_params' => [
                    'customerId' => $user->id,
                    'iban' => $user->IBAN,
                    'owner' => $user->account_owner
                    ]
                ]);
                $bodyResponse = \GuzzleHttp\json_decode($result->getBody()->getContents(), true);
                try {
                    $paymentData = new PaymentData();
                    $paymentId = $bodyResponse['paymentDataId'];
                    $paymentData->user_id = $user->id;
                    $paymentData->payment_data_id = $paymentId;
                    $paymentData->save();

                    // remove sessions
                    session()->forget('firstForm');
                    session()->forget('secondForm');
                    session()->forget('thirdForm');
                    session()->forget('active');
                    return redirect()->route('successPage')->with('paymentDataId', $paymentId);
                } catch (\Exception $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        } catch (\Exception $e) {
            return redirect()->route('errorPage')
            ->with('line', $e->getLine())
            ->with('file', $e->getFile())
            ->with('message', $e->getMessage())
            ->with('code', $e->getCode());
        }



    }

    public function getStepFourError() {
        return view('error-page');
    }

    public function getStepFourSuccess() {
        return view('success-page');
    }


}
