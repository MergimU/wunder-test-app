@extends('structure.layout')
@section('title', 'View 2')

@section('content')
    <br>
    <div class="container">
        <h3>Step 2</h3>
        <hr>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form method="post" action="{{route('stepTwo')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" aria-describedby="addressHelp"  placeholder="Enter address"
                               value="{{ $data ? $data->address : ''}}" required>
                        <small id="addressHelp" class="form-text text-muted"></small>
                    </div>
                    <div class="form-group">
                        <label for="zip-code">Zip Code</label>
                        <input type="text" class="form-control" id="zip-code" name="zipcode"  placeholder="Enter zip code" value="{{$data ? $data->zipcode : ''}}" required>
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" name="city" placeholder="Enter city if not provided from google"
                               value="{{$data ? $data->city : ''}}" required>
                    </div>
                    <div class="form-group">
                        <label for="house-number">House Number</label>
                        <input type="text" class="form-control" id="house-number" name="houseNumber" placeholder="Enter your house number"
                               value="{{$data ? $data->houseNumber : ''}}" required>
                    </div>
                    <a href="{{route('getFirst')}}" class="btn btn-primary">Back</a>
                    <button type="submit" class="btn btn-primary">Next</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('page-scripts')

    <script src="{{asset('assets/js/google-autocomplete.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfT7QnOT6HHqGN6KqPnhWVx3vHkiYaO8M&libraries=places&callback=initAutocomplete"
            async defer></script>
    @endsection
