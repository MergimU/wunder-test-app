<!DOCTYPE html>
<html lang="en">
<head>
    <title>Wunder | @yield('title')</title>
    @include('structure.styles')
    @yield('page-styles')
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand mb-0 h1">Wunder Test App</span>
</nav>

@yield('content')

@include('structure.scripts')
@yield('page-scripts')
</body>
</html>
