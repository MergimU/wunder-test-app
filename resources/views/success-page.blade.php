


@extends('structure.layout')

@section('title', 'Success')

@section('content')
    <br><br><br>
    <hr>
    <div class="alert alert-success">
        <strong>Success!</strong> Payment is done successfully! Payment ID: {{session('paymentDataId') ? session('paymentDataId') : 'Payment ID is not provided'}}
    </div>
    <hr>


@endsection
