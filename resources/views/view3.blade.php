@extends('structure.layout')
@section('title', 'View 3')

@section('content')
    <br>
    <div class="container">
        <h3>Step 3</h3>
        <hr>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form action="{{route('stepThree')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="owner">Account Owner</label>
                        {{--<input type="hidden" class="form-control" value="1" name="customerId" placeholder="Enter accout owner" required>--}}
                        <input type="text" class="form-control" id="owner" name="owner" value="{{$data ? $data->owner : ''}}" placeholder="Enter accout owner" required>
                    </div>
                    <div class="form-group">
                        <label for="iban">IBAN</label>
                        <input type="text" class="form-control" id="iban" name="IBAN" value="{{$data ? $data->IBAN : ''}}" placeholder="Enter IBAN" required>
                    </div>
                    <a href="{{route('getSecond')}}" class="btn btn-primary">Back</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>

@endsection
