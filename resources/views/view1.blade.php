@extends('structure.layout')
@section('title', 'View 1')

@section('content')
    <br>
<div class="container">
    <h3>Step 1</h3>
    <hr>
    <div class="row">
       <div class="col-md-6 offset-md-3">
           <form action="{{route('stepOne')}}" method="post">
               {{csrf_field()}}

               <div class="form-group">
                   <label for="name">Name</label>
                   <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="{{$data ? $data->name : ''}}" required>
               </div>
               <div class="form-group">
                   <label for="last-name">Last Name</label>
                   <input type="text" class="form-control" id="last-name" name="lastname" value="{{$data ? $data->lastname : ''}}" placeholder="Enter last name" required>
               </div>
               <div class="form-group">
                   <label for="phone">Telephone</label>
                   <input type="text" class="form-control" id="phone" name="phone" minlength="9" maxlength="20" value="{{$data ? $data->phone : ''}}" placeholder="Enter telephone number" required>
               </div>
               <button type="submit" class="btn btn-primary">Next</button>
           </form>
       </div>
    </div>
</div>

    @endsection
