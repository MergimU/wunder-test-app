@extends('structure.layout')

@section('title', 'Error')

@section('content')
    <hr>
    <h1>ERROR</h1>
    <hr>
    <div class="d-flex justify-content-left align-items-center" id="main">
        <h4 class="mr-3 pr-3 align-top border-right inline-block align-content-center">{{session('code') ? session('code') : 'Error'}}</h4>
        <div class="inline-block align-middle">
            <h3 class="font-weight-normal lead" id="desc">{{session('message') ? session('message') : 'Unknown message'}}</h3>
        </div>
    </div>

    <div class="d-flex justify-content-left align-items-center" id="main">
        <h4 class="mr-3 pr-3 align-top border-right inline-block align-content-center">File</h4>
        <div class="inline-block align-middle">
            <h3 class="font-weight-normal lead" id="desc">{{session('file') ? session('file') : 'Unknown File'}}</h3>
        </div>
    </div>
    <div class="d-flex justify-content-left align-items-center" id="main">
        <h4 class="mr-3 pr-3 align-top border-right inline-block align-content-center">Line</h4>
        <div class="inline-block align-middle">
            <h3 class="font-weight-normal lead" id="desc">{{session('line') ?  session('line') : 'Unknown File'}}</h3>
        </div>
    </div>

    @endsection
