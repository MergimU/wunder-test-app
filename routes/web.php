<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;

Route::get('/', function () {
    if (session('active')) {
        switch (session('active')) {
            case 2:
                return redirect()->route('getSecond');
                break;
            case 3:
                return redirect()->route('getThird');
                break;
        }
    } else { return redirect()->route('getFirst'); }
});
/*
|--------------------------------------------------------------------------
|Page one routes
|--------------------------------------------------------------------------
*/
    Route::get('step-one', 'UserController@getStepOne')->name('getFirst');
    Route::post('step-one', 'UserController@postStepOne')->name('stepOne');


/*
|--------------------------------------------------------------------------
|Page two routes
|--------------------------------------------------------------------------
*/
    Route::get('step-two', 'UserController@getStepTwo')->name('getSecond');
    Route::post('step-two', 'UserController@postStepTwo')->name('stepTwo');


/*
|--------------------------------------------------------------------------
|Page three routes
|--------------------------------------------------------------------------
*/
    Route::get('step-three', 'UserController@getStepThree')->name('getThird');
    Route::post('step-three', 'UserController@postStepThree')->name('stepThree');


/*
|--------------------------------------------------------------------------
|Page four routes
|--------------------------------------------------------------------------
*/
    Route::get('step-four-success', 'UserController@getStepFourSuccess')->name('successPage');
    Route::get('step-four-error', 'UserController@getStepFourError')->name('errorPage');
