# wunder-test-app

## 1. Describe possible performance optimizations for your Code.

- Route caching
	Route caching helps for better perfomance of an application. Even if we do not have many roots, it helps and its essential for better performance. Route caching is simply an array and it improves perfomance because of loading faster the array
	
- Confing caching
	Laravel provides artisan command for caching configuration that helps to boost performance of application. Once the configurations are cached, next changes in configuration do not have affects till next chache
	
- Remove unused services, packages and libraries



## 2. Which things could be done better, than you�ve done it?
 
- Using more isset function
- using (===) rather than (==)


### Explanation

I do not include an export of database, because whole tables and structure is in migrations file. 
All you need to do is to create database in phpmyadmin, then the name of database you include in .env file
After success connection with database, run: php artisan migrate