function initAutocomplete() {
//get search input for location
var input = document.getElementById('address');



//option object for different properties such as restricion addresses, cities etc
var options = {
    types: ['address'],
    componentRestrictions: {country: 'de'}
};

//initialize autocomplete function
var autocomplete = new google.maps.places.Autocomplete(input,options);
//event when new location is typed
autocomplete.addListener('place_changed', function() {

    //get place attributes from getplace method
    var place = autocomplete.getPlace();
    var zip_code = document.getElementById('zip-code').value;
    var city = document.getElementById('city').value;
    var street;
    for (var i = 0; i < place.address_components.length; i++) {
        for (var j = 0; j < place.address_components[i].types.length; j++) {
            if (place.address_components[i].types[j] == "postal_code") {
                zip_code = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[j] == "locality") {
                city = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[j] == "street_number") {
                street = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[j] == "route") {
                input = place.address_components[i].long_name;
            }
        }
    }
    if (street === undefined) {
        document.getElementById('addressHelp').innerText = 'Please include your street number!';
        street = '';
    } else {
        document.getElementById('addressHelp').innerText = '';

    }

    document.getElementById('address').value = input + ' ' + street;
    document.getElementById('city').value = city;
    document.getElementById('zip-code').value = zip_code;
});
}


